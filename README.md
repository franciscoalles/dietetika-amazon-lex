# PFI-Dietetika (Amazon Lex)
Bienvenido al proyecto de Dietetika!
Este módulo representa al bot de Amazon Lex, el cual interactua con AWS Amplify y las APIs del Backend.
---
REQUISITOS
-
Para lavantar el proyecto se debe cumplir con los siguientes requisitos:

- Instancia Amazon Web Services
- Servicio de Amazon Lex
---
Ejecución del proyecto
-
1. Abrir consola de Amazon Lex
2. Seleccionar opción de importar
3. Seleccionar archivo .zip o bien .json
4. Modificar configuraciones de integración de la app para conectarse con el servicio de Amazon Lex